{ config, lib, pkgs, modulesPath, ... }:

{
  config = {
    ###
    # User Passwords
    ###
    users.users = {
      # mkpasswd -m sha-512
      root = {
        hashedPassword = "!";
      };
      afontain = {
        hashedPassword = "!";
      };
      working = {
        hashedPassword = "!";
      };
    };
  };
}
