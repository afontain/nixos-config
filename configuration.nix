# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ ./hardening.nix
      ./secrets.nix
      ./components/cups.nix
      ./components/documentation.nix
      ./components/gnome.nix
      ./components/large-graphical-software.nix
      ./components/locale.nix
      ./components/network.nix
      ./components/remarkable.nix
      ./components/tools.nix
      ./components/unvanquished.nix
      ./components/vim.nix
      ./components/virtualisation.nix
      ./components/zsh.nix
    ];

  config = {

    ###
    # System Packages
    ###
    environment.systemPackages = with pkgs; [
      nix-doc
      nix-index
    ];

    services.udev.packages = [ pkgs.platformio-core.udev ];

    ###
    # User Accounts
    ###
    users.mutableUsers = false;
    users.defaultUserShell = pkgs.zsh;
    users.users.root = {
      isNormalUser = false;
      home = "/root";
    };
    users.users.working = {
      isNormalUser = true;
      home = "/home/afontain-working";
      description = "Antoine Fontaine at Work";
      extraGroups = [ "wheel" "networkmanager" ];
    };
    users.users.afontain = {
      isNormalUser = true;
      home = "/home/afontain";
      description = "Antoine Fontaine";
      extraGroups = [ "wheel" "networkmanager" "dialout" ];
    };
  
    services.nscd.enableNsncd = false;

    ###
    # Services
    ###


    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    programs.mtr.enable = true;
    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  
    ## ridiculous hack for the time tables
    #services.darkhttpd = {
    #  enable = true;
    #  port = 8009;
    #  address = "127.0.0.1";
    #  rootDir = /srv/darkhttpd-edt;
    #  #extraArgs = [ "--chroot" ]; # broken: chroot syscall isn't allowed (¬root)
    #};
  
  
    ###
    # Channels
    ###
  
    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "22.05"; # Did you read the comment?
  
    #system.autoUpgrade.enable = true;
    #system.autoUpgrade.allowReboot = true;
    #system.autoUpgrade.channel = https://nixos.org/channels/nixos-21.05;
  };
}
