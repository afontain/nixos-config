{
  description = "NixOS configuration with two or more channels";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.05";
    nixpkgs-old.url = "nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, nixpkgs-old, nixpkgs-unstable }:
    let
      system = "x86_64-linux";
      allowUnfreePredicate = pkg: nixpkgs.lib.assertMsg
        (builtins.elem (nixpkgs.lib.getName pkg) [ "zoom" "geogebra" "steam-original" "steam-run" ])
        (nixpkgs.lib.getName pkg + " is nonfree");
      pkgs = import nixpkgs {
        inherit system;
        config = { inherit allowUnfreePredicate; };
        overlays = [ (final: prev: {
          pkgs-unstable = import nixpkgs-unstable {
            inherit system;
            config = { inherit allowUnfreePredicate; };
          };
          pkgs-old = import nixpkgs-old {
            inherit system;
            config = { inherit allowUnfreePredicate; };
          };
        }) ];
      };
      hostnames = [ "thinkpad-x-ccxxx-f" ];

    in {
      nixosConfigurations = nixpkgs.lib.attrsets.genAttrs hostnames (hostname:
        nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            {
              imports = [
                ./configuration.nix
                (./. + builtins.toPath "/hardware-configuration-${hostname}.nix")
              ];
              nix = {
                # flakes
                extraOptions = "experimental-features = nix-command flakes";
                settings.sandbox = true;

                daemonIOSchedClass = "idle";
                daemonIOSchedPriority = 7;
                daemonCPUSchedPolicy = "idle";

                registry."nixpkgs".flake = nixpkgs; # use system nixpkgs everywhere
                registry."nixpkgs-old".flake = nixpkgs-old;
                registry."nixpkgs-unstable".flake = nixpkgs-unstable;
                # abbreviations
                registry."p".flake = nixpkgs;
                registry."pu".flake = nixpkgs-unstable;
              };
              nixpkgs.pkgs = pkgs;
            }
          ];
        });
    pkgs = pkgs;
  };
}
