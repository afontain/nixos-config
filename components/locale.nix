{ config, pkgs, lib, ... }:
{
  config = {
    # Set your time zone.
    time.timeZone = "Europe/Zurich";
  
    # Select internationalisation properties.
    i18n.defaultLocale = "en_GB.UTF-8";
    console = {
      font = "Lat2-Terminus16";
      keyMap = "fr-bepo";
    };

    services.xserver = {
      layout = "fr";
      xkbVariant = "bepo";
    };
  };
}
