{ config, pkgs, lib, ... }:
{
  config = {
    ###
    # Documentation
    ###

    documentation = {
      dev.enable = true; # developer doc
      man.generateCaches = true; # allows searching using apropos (but this may add too much compilation time?)
      info.enable = false; # I mean, it's not like info is usable or anything
    };

    environment.systemPackages = with pkgs; [
      posix_man_pages man-pages
    ];
  };
}
