{ config, pkgs, lib, ... }:
{
  config = {
    ###
    # Virtualisation and software dev
    ###
    boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

    programs.adb.enable = true;

    virtualisation.docker.rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };
}
