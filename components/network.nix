{ config, pkgs, lib, ... }:

{
  config = {
    environment.systemPackages = with pkgs; [
      wireguard-tools
    ];

    networking = {
      networkmanager.enable = true;

      firewall = {
        enable = true;
        allowedTCPPorts = [];
        allowedUDPPorts = [
          51820 # wireguard
        ];
        allowPing = true;
      };
    };

    ## Enable WireGuard
    #networking.wireguard.interfaces = {
    #  # "wg0" is the network interface name. You can name the interface arbitrarily.
    #  wg0 = {
    #    # Determines the IP address and subnet of the client's end of the tunnel interface.
    #    ips = [ "192.168.123.128/24" ];
    #    listenPort = 51820; # to match firewall allowedUDPPorts (without this wg uses random port numbers)

    #    # Path to the private key file.
    #    #
    #    # Note: The private key can also be included inline via the privateKey option,
    #    # but this makes the private key world-readable; thus, using privateKeyFile is
    #    # recommended.
    #    privateKeyFile = "/etc/nixos/wg-private";

    #    peers = [
    #      # For a client configuration, one peer entry for the server will suffice.

    #      {
    #        # Public key of the server (not a file path).
    #        publicKey = "aA10jDJXlP3FbW5HSMGq96qa1R9bjJi1ynfzsCaaph0=";

    #        # Or forward only particular subnets
    #        allowedIPs = [ "192.168.122.0/24" "192.168.123.0/24" ]; # "192.26.40.0/24" 

    #        # Set this to the server IP and port.
    #        endpoint = "192.26.40.10:51820"; # ToDo: route to endpoint not automatically configured https://wiki.archlinux.org/index.php/WireGuard#Loop_routing https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577

    #        # Send keepalives every 25 seconds. Important to keep NAT tables alive.
    #        persistentKeepalive = 25;
    #      }
    #    ];

    #    postSetup = ''
    #      ip route add 192.168.122.0/24 via 192.168.123.1 dev wg0
    #      # add rapperswil DNS
    #      #echo "nameserver 192.26.40.13" | ${pkgs.systemd}/bin/resolvconf -a wg0 -m 0
    #    '';
    #  };
    #};
  };
}
