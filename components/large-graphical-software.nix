{ config, pkgs, lib, ... }:

{
  config = {
    environment.systemPackages = with pkgs; [
      # graphical apps
      xournalpp libreoffice octaveFull # office
      gimp inkscape krita
      freemind

      # multimedia
      obs-studio
      kdenlive
      gnome.cheese

      kicad

      # icky stuff
      pkgs-unstable.zoom-us
      steam-run-native
      geogebra
    ];

    fonts.packages = with pkgs; [
      libertine
      libertinus
      junicode
      eunomia
      #nerdfonts
      #xorg.fontadobeutopiatype1
      #xorg.fontbhttf
      ferrum
      clearlyU
      luculent
      roboto
      roboto-serif
      paratype-pt-serif
      paratype-pt-sans
      paratype-pt-mono
      open-sans
    ];

    services.flatpak.enable = true;
  };
} 
