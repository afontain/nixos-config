
{ config, pkgs, lib, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      silver-searcher
      git tig
      tmux
      tree htop file
      zsh zoxide # shell
      direnv
    ];

    programs.zsh = {
      enable = true;
      ohMyZsh = {
        enable = true;
        theme = "alanpeabody";
        plugins = [ "git" "tig" "wd" ];
      };
      syntaxHighlighting = {
        enable = true;
      };
      # man zshoptions
      setOptions = [ "HIST_IGNORE_ALL_DUPS" "HIST_IGNORE_SPACE" ];
      interactiveShellInit = ''
        _ZO_MAXAGE=100000  _ZO_ECHO=1 eval "$(zoxide init zsh)"
        REPORTTIME=10
        alias g="git"
        for cmd in push pull commit stash rebase merge rebase checkout switch branch bisect; do
          alias $cmd="git $cmd"
        done
        alias pop="git stash pop"
        alias g="git"
        alias flake="nix flake"
      '';
      histSize = 100000;
    };

    environment.loginShellInit = ''
      readonly TMOUT=300 # autologout time in second
      if [ "$XDG_SESSION_TYPE" = tty ] && [ -z "$TMUX" ]
      then
        exec ${pkgs.tmux}/bin/tmux
      fi
    '';

    programs.git.enable = true;
    programs.git.config = {
      pull = {
        ff = "only";
      };
      color = {
        ui = "auto";
      };
      alias = {
        c = "commit";
        st = "status --short";
        ctmp = "!git commit -a -mTMP && git show --stat";
        # all commits unreachable via branch, tag, or child commit
        # ignores anything pointed to by the reflog
        # so it displays all commits in jeopardy of garbage collection
        loose-commits = ''
          !"for SHA in $(git fsck --unreachable\
              --no-reflogs | grep commit |\
              cut -d\\  -f 3); do git log -n 1 $SHA; \
            done"
        '';
      };
      init = {
        defaultBranch = "main";
      };
      safe = {
        directory = "/etc/nixos";
      };
      advice = {
        pushNonFastForward = "false";
        statusHints = "false";
      };
      diff = {
        renames = "copies";
        algorithm = "histogram";
      };
      push = {
        default = "tracking";
      };
      rerere = {
        enabled = "true";
      };
      merge = {
        stat = "true";
      };
      rebase = {
        stat = "true";
        missingCommitsCheck = "error";
        rescheduleFailedExec = "true";
      };
    };

    services.lorri.enable = true;
  };
}
