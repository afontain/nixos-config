{ config, pkgs, lib, ... }:

# useful command line crap that is technically not required or considered as
# the basis of a working shell, but that's still quite useful

{
  config = {
    environment.systemPackages = with pkgs; [
      pass pass-wayland
      rsync
      imagemagick
      dash
      ranger
      ctags
      lm_sensors
      cmakeCurses
      jq
      unzip
      p7zip
      binwalk
      strace gdb
      binutils-unwrapped
      wget curl aria2 # downloading
      moreutils # `sponge`, …
      whois
      bind # for the dig command
      nmap
      usbutils # lsusb
      bubblewrap
      git-annex
      #minicom
      btrfs-progs
      ntfs3g ntfsprogs
    ];

    programs.light.enable = true;
  };
} 
