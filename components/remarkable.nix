{ config, pkgs, lib, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      lz4
      ffmpeg
      libnotify
      rmapi
    ];

    # remarkable magic for mouse
    # CHECKME: is this useful
    services.udev.extraHwdb = ''
      evdev:input:b0018v056Ap0000*
       EVDEV_ABS_00=::20
       EVDEV_ABS_01=::20
    '';
  };
}
