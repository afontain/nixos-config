{ config, pkgs, lib, ... }:

let gaspar = "afontain";
in {
  config = {
    environment.systemPackages = with pkgs; [
      adw-gtk3 # theme. Use `gsettings set org.gnome.desktop.interface gtk-theme adw-gtk3-dark`

      gnome.devhelp meld nemiver # dev
      gnome.vinagre # vnc
      gnome.gnome-chess gnome.gnome-mines gnome.gnome-sudoku # games
      gnome.gnome-terminal

      wl-clipboard xclip

      mpv yt-dlp

      # other important gui apps
      tdesktop
      firefox
    ] ++ (with pkgs.gnomeExtensions;
    [
      caffeine
      #material-you-color-theming
      #thinkpad-battery-thresholds
    ]);

    programs.hamster.enable = true;

    programs.evolution.enable = true;
    programs.evolution.plugins = [ pkgs.evolution-ews ];
    services.gnome.evolution-data-server.enable = true;

    services.xserver = {
      enable = true;
      displayManager = {
        gdm.enable = true;
        #autoLogin = {
        #  enable = true;
        #  user = "afontain";
        #};
        defaultSession = "gnome";
      };
      desktopManager.gnome = {
        enable = true;
        extraGSettingsOverrides = ''
          [org.gnome.settings-daemon.plugins.color]
          night-light-enabled=true
          night-light-temperature=1000
  
          [org.gnome.mutter]
          attach-modal-dialogs=true
          dynamic-workspaces=true
          edge-tiling=true
          focus-change-on-pointer-rest=true
          workspaces-only-on-primary=true
  
          [org.gnome.desktop.wm.preferences]
          audible-bell=false
          focus-mode='sloppy'
  
          [org.gnome.software]
          download-updates=false
          download-updates-notify=false
          first-run=false
  
          [org.gtk.Settings.Debug]
          enable-inspector-keybinding=true
  
          [org.gnome.desktop.sound]
          allow-volume-above-100-percent=true
  
          [org.gnome.nautilus.list-view]
          default-zoom-level='standard'
  
          #org.gnome.desktop.peripherals.tablet keep-aspect false
          #org.gnome.desktop.peripherals.tablet area [0.0, 0.0, 0.0, 0.0]
          #org.gnome.desktop.peripherals.tablet left-handed false
          #org.gnome.desktop.peripherals.tablet mapping 'absolute'
  
          [org.gnome.desktop.interface]
          gtk-theme='Adwaita-dark'
          clock-show-weekday=true

          [org.gnome.epiphany]
          ask-for-default=false
          
          [org.gnome.settings-daemon.plugins.media-keys.custom-keybindings.custom0]
          binding='<Super>x'
          command='gnome-terminal'
          name='Terminal'
          
          [org.gnome.settings-daemon.plugins.media-keys.custom-keybindings.custom1]
          binding='<Super>agrave'
          command='passmenu'
          name='Pass menu'
          
          [org.gnome.Connections]
          first-run=false
          
          [org.gnome.builder]
          window-maximized=true

          [org.gnome.builder.editor]
          overscroll=10
          style-scheme-name='Adwaita-dark'
          
          [ch.gnugen.Moody]
          username='${gaspar}'
        '';
      };
  
      # intel driver
      videoDrivers = [ "modesetting" ];
      # for games
      #hardware.opengl.driSupport32Bit = true;
  
      wacom.enable = true;
  
      libinput.enable = true;
      libinput.touchpad.tapping = false;
    };

    # avoid some GNOME bloat
    environment.gnome.excludePackages =
      (with pkgs;
        [ gnome-tour yelp orca ]) ++
      (with pkgs.gnome;
        [ sushi totem gnome-software gnome-packagekit ]);

    services.gnome.gnome-initial-setup.enable = false;

    services.flatpak.enable = true;
  };
} 
