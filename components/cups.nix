
{ config, pkgs, lib, ... }:

{
  config = {
    # Enable CUPS
    services.printing.enable = true;
    services.printing.browsing = true;
    # and avahi for autodiscovery
    services.avahi.enable = true;
    services.avahi.nssmdns = true;

    ## Add EPFL printers (broken)
    #hardware.printers.ensurePrinters = [
    #  {
    #    name = "EPFL-BW";
    #    description = "EPFL Black-and-White";
    #    deviceUri = "lpd://${gaspar}@printepfl1.epfl.ch/SecurePrint-BW";
    #    model = "raw";
    #    # Doesn't work
    #    #deviceUri = "ipp://printepfl1.epfl.ch/SecurePrint-BW";
    #    #model = "everywhere";
    #    location = "EPFL";
    #    ppdOptions = {};
    #    # TODO: color
    #  }
    #];
  };
}
