{ config, pkgs, lib, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      vim vimPlugins.vim-addon-nix
    ];

    programs.vim = {
      defaultEditor = true;
    };
  };
}
