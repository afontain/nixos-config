# Hardening options
# They will probably break a thing or two.
{ config, lib, pkgs, modulesPath, ... }:

{
  security.sudo.enable = false;
  # apparmor confinement
  security.apparmor.enable = true;

  nix.settings.allowed-users = [ "@wheel" ];

  environment.defaultPackages = lib.mkForce [];

  ####
  ## Filesystems
  ####
  #
  ## WARN: breaks pmbootstrap
  #fileSystems."/tmp" =
  #  { device = "/tmp";
  #    fsType = "bind";
  #    options = [ "bind" "nosuid" "noexec" "nodev" ];
  #  };
  #
  #fileSystems."/var/tmp" =
  #  { device = "/var/tmp";
  #    fsType = "bind";
  #    options = [ "bind" "nosuid" "noexec" "nodev" ];
  #  };
  #
  ## WARN: breaks unvanquished
  #fileSystems."/dev/shm" =
  #  { device = "none";
  #    fsType = "tmpfs";
  #    options = [ "rw" "nosuid" "noexec" "nodev" ];
  #  };


  ###
  # sysfs
  ###

  boot.kernel.sysctl."kernel.yama.ptrace_scope" = 1;
  boot.kernel.sysctl."kernel.dmesg_restrict" = 1;
  boot.kernel.sysctl."kernel.unprivileged_bpf_disabled" = 1;
  boot.kernel.sysctl."fs.suid_dumpable" = 0;
}
